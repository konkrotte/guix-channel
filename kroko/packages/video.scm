;;; GNU Guix --- Functional package management for GNU
;;;
;;; Copyright © 2021 konkrotte <konkrotte@posteo.net>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (kroko packages video)
  #:use-module (gnu packages)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages video)
  #:use-module (gnu packages image)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages assembly)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public vapoursynth-mvtools
  (package
    (name "vapoursynth-mvtools")
    (version "23")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/dubhater/vapoursynth-mvtools")
	     (commit (string-append "v" version))))
       (sha256
	(base32 "0lngkvxnzn82rz558nvl96rvclrck07ja1pny7wcfixp9b68ppkn"))))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
	 (replace 'install
	   (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
	       (install-file "libmvtools.so" (string-append out "/lib/vapoursynth"))))))))
    (build-system meson-build-system)
    (native-inputs
     (list
      pkg-config
      nasm))
    (inputs
     (list
      fftwf
      vapoursynth
      zimg))
    (home-page "https://github.com/dubhater/vapoursynth-mvtools")
    (synopsis "Motion compensation and stuff")
    (description "MVTools is a set of filters for motion estimation and compensation.")
    (license license:gpl2)))

(define-public ani-cli
  (package
    (name "ani-cli")
    (version "3.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/pystardust/ani-cli/")
	     (commit (string-append "v" version))))
       (sha256
	(base32 "0k618dl0mdnhnpywy6aydidrf5pmc6k1bdykmazzav7yjmqalb1d"))))
    (build-system copy-build-system)
    (inputs
     (list
      ncurses
      ffmpeg))
    (arguments
     `(#:install-plan
       `(("bin/ani-cli" "bin/")
         ("lib/" "lib/"))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'patch
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* '("bin/ani-cli" "lib/ani-cli/player_download")
               (("ffmpeg") (string-append (assoc-ref inputs "ffmpeg") "/bin/ffmpeg"))
               (("tput") (string-append (assoc-ref inputs "ncurses") "/bin/tput"))))))))
    (home-page "https://github.com/pystardust/ani-cli")
    (synopsis "")
    (description "")
    (license license:gpl3+)))
