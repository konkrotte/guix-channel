;;; GNU Guix --- Functional package management for GNU
;;;
;;; Copyright © 2021 konkrotte <konkrotte@posteo.net>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (kroko packages linux)
  #:use-module (gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages markup)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gnunet)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages check)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages nss)
  #:use-module (gnu packages man)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages perl)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:))

(define-public bees
  (let ((version "0.7"))
    (package
      (name "bees")
      (version version)
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/Zygo/bees")
	       (commit (string-append "v" version))))
	 (file-name (git-file-name name version))
	 (sha256
	  (base32 "0nk32c9rvggdrf53g4pxkd489blij7ml0fjk5zm06hk7qjjbygl4"))))
      (build-system gnu-build-system)
      (arguments
       `(#:phases
	 (modify-phases %standard-phases
	   (delete 'configure))))
      (native-inputs
       (list
	markdown
	gcc-toolchain-11
	pkg-config))
      (inputs
       (list
	btrfs-progs))
      (home-page "https://github.com/Zygo/bees")
      (synopsis "Best-Effort Extent-Same, a btrfs dedupe agent")
      (description "bees is a block-oriented userspace deduplication agent designed for large btrfs filesystems. It is an offline dedupe combined with an incremental data scan capability to minimize time data spends on disk from write to dedupe.")
      (license license:gpl3+))))

(define-public gnunet-messenger-cli
  (package
    (name "gnunet-messenger-cli")
    (version "0.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://git.gnunet.org/git/messenger-cli.git")
	     (commit "e56259e8766ea097e4f2b841584e2f6b20534464")))
       (sha256
	(base32 "19ahqm1g6gpjh86fpmv0jxv9pbs2i31rsnzr0i93lvn8jclk3mnm"))))
    (inputs
     (list ncurses
	   libgnunetchat
	   gnunet
	   libsodium
	   libextractor
	   libgcrypt))
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
	 (delete 'configure)
	 (add-before 'install 'fix-install-path
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
	       (substitute* '("Makefile")
                 (("INSTALL_DIR \\?= .+") (string-append "INSTALL_DIR ?= " out "/\n")))
	       (invoke "mkdir" "-p" (string-append out "/bin"))))))))
    (build-system gnu-build-system)
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libgnunetchat
  (package
    (name "libgnunetchat")
    (version "0.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://git.gnunet.org/git/libgnunetchat.git")
	     (commit (string-append "v" version))))
       (sha256
	(base32 "081p6w3rsyq4a1gkxkcdka6vy51p6p3qsnqn0b1alhnlcaxc56d6"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
	 (delete 'configure)
	 (add-before 'install 'fix-install-path
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
	       (substitute* '("Makefile")
                 (("INSTALL_DIR \\?= .+") (string-append "INSTALL_DIR ?= " out "/\n")))
	       (invoke "mkdir" "-p" (string-append out "/lib"))))))))
    (inputs
     (list gnunet
	   libsodium
	   libgcrypt
	   libextractor))
    (native-inputs
     (list check))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))


(define-public nss-next
  (package
    (name "nss")
    ;; Also update and test the nss-certs package, which duplicates version and
    ;; source to avoid a top-level variable reference & module cycle.
    (version "3.78")
    (source (origin
              (method url-fetch)
              (uri (let ((version-with-underscores
                          (string-join (string-split version #\.) "_")))
                     (string-append
                      "https://ftp.mozilla.org/pub/mozilla.org/security/nss/"
                      "releases/NSS_" version-with-underscores "_RTM/src/"
                      "nss-" version ".tar.gz")))
              (sha256
               (base32
                "0048lqnxfx0qd94adpb6a1cpsmcsggvq82p851ridhc7wx0z6mgl"))
              ;; Create nss.pc and nss-config.
              (patches (search-patches "nss-3.56-pkgconfig.patch"
                                       "nss-getcwd-nonnull.patch"
                                       "nss-increase-test-timeout.patch"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  ;; Delete the bundled copy of these libraries.
                  (delete-file-recursively "nss/lib/zlib")
                  (delete-file-recursively "nss/lib/sqlite")))))
    (build-system gnu-build-system)
    (outputs '("out" "bin"))
    (arguments
     `(#:make-flags
       (let* ((out (assoc-ref %outputs "out"))
              (nspr (string-append (assoc-ref %build-inputs "nspr")))
              (rpath (string-append "-Wl,-rpath=" out "/lib/nss")))
         (list "-C" "nss" (string-append "PREFIX=" out)
               "NSDISTMODE=copy"
               "NSS_USE_SYSTEM_SQLITE=1"
               ;; The gtests fail to compile on riscv64.
               ;; Skipping them doesn't affect the test suite.
               ,@(if (target-riscv64?)
                     `("NSS_DISABLE_GTESTS=1")
                     '())
               (string-append "NSPR_INCLUDE_DIR=" nspr "/include/nspr")
               ;; Add $out/lib/nss to RPATH.
               (string-append "RPATH=" rpath)
               (string-append "LDFLAGS=" rpath)))
       #:modules ((guix build gnu-build-system)
                  (guix build utils)
                  (ice-9 ftw)
                  (ice-9 match)
                  (srfi srfi-26))
       #:tests? #f
       ;; ,(not (or (%current-target-system)
       ;;                    ;; Tests take more than 30 hours on riscv64-linux.
       ;;                    (target-riscv64?)))
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda _
             (setenv "CC" ,(cc-for-target))
             ;; Tells NSS to build for the 64-bit ABI if we are 64-bit system.
             ,@(if (target-64bit?)
                   `((setenv "USE_64" "1"))
                   '())))
         (replace 'check
           (lambda* (#:key tests? #:allow-other-keys)
             (if tests?
                 (begin
                   ;; Use 127.0.0.1 instead of $HOST.$DOMSUF as HOSTADDR for
                   ;; testing.  The latter requires a working DNS or /etc/hosts.
                   (setenv "DOMSUF" "localdomain")
                   (setenv "USE_IP" "TRUE")
                   (setenv "IP_ADDRESS" "127.0.0.1")

                   ;; The "PayPalEE.cert" certificate expires every six months,
                   ;; leading to test failures:
                   ;; <https://bugzilla.mozilla.org/show_bug.cgi?id=609734>.  To
                   ;; work around that, set the time to roughly the release date.
                   (invoke "faketime" "2021-09-30" "./nss/tests/all.sh"))
                 (format #t "test suite not run~%"))))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append (assoc-ref outputs "bin") "/bin"))
                    (inc (string-append out "/include/nss"))
                    (lib (string-append out "/lib/nss"))
                    (obj (match (scandir "dist" (cut string-suffix? "OBJ" <>))
                           ((obj) (string-append "dist/" obj)))))
               ;; Install nss-config to $out/bin.
               (install-file (string-append obj "/bin/nss-config")
                             (string-append out "/bin"))
               (delete-file (string-append obj "/bin/nss-config"))
               ;; Install nss.pc to $out/lib/pkgconfig.
               (install-file (string-append obj "/lib/pkgconfig/nss.pc")
                             (string-append out "/lib/pkgconfig"))
               (delete-file (string-append obj "/lib/pkgconfig/nss.pc"))
               (rmdir (string-append obj "/lib/pkgconfig"))
               ;; Install other files.
               (copy-recursively "dist/public/nss" inc)
               (copy-recursively (string-append obj "/bin") bin)
               (copy-recursively (string-append obj "/lib") lib)))))))
    (inputs
     (list sqlite zlib))
    (propagated-inputs
     (list nspr))                 ;required by nss.pc.
    (native-inputs
     (list perl libfaketime))   ;for tests

    ;; The NSS test suite takes around 48 hours on Loongson 3A (MIPS) when
    ;; another build is happening concurrently on the same machine.
    (properties '((timeout . 216000)))  ;60 hours

    (home-page "https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS")
    (synopsis "Network Security Services")
    (description
     "Network Security Services (@dfn{NSS}) is a set of libraries designed to
support cross-platform development of security-enabled client and server
applications.  Applications built with NSS can support SSL v2 and v3, TLS,
PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509 v3 certificates, and other
security standards.")
    (license license:mpl2.0)))

(define-public pesign
  (package
    (name "pesign")
    (version "115")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
	     "https://github.com/rhboot/pesign/releases/download/"
	     version "/pesign-" version ".tar.bz2"))
       (sha256
	(base32 "0zphri0iyqbsrwvs0n3c7r6v7133h0x4v53nm3r7bq9dp1hza7mc"))
       (patches (list (local-file "../patches/pesign-guix.patch")))))
    (build-system gnu-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (delete 'configure)
		  (delete 'check)
		  (add-after 'patch-generated-file-shebangs 'patch-makefile
		    (lambda _
		      (substitute* "Make.defaults"
			(("-Werror") "")
			(("= /usr/") "= /")))))
       #:make-flags
       (let ((out (assoc-ref %outputs "out")))
	 (list (string-append "DESTDIR=" out)))))
    (inputs `(("popt" ,popt)
	      ("efivar" ,efivar)
	      ("nss" ,nss-next)
	      ("mandoc" ,mandoc)
	      ("util-linux:lib" ,util-linux "lib")))
    (native-inputs (list pkg-config))
    (synopsis "Linux tools for signed PE-COFF binaries")
    (description
     "Signing tools for PE-COFF binaries. Compliant with the PE and Authenticode specifications.
      (These serve a similar purpose to Microsoft's SignTool.exe, except for Linux.)")
    (home-page "https://github.com/rhboot/pesign")
    (license license:gpl3+)))

