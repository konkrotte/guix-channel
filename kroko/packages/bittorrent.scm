;;; GNU Guix --- Functional package management for GNU
;;;
;;; Copyright © 2021 konkrotte <konkrotte@posteo.net>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (kroko packages bittorrent)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages bittorrent)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages nss)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:))

(define-public btfs
  (package
    (name "btfs")
    (version "2.24")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/johang/btfs")
	     (commit (string-append "v" version))))
       (sha256
	(base32 "1b4g6aas0k6i8f43sjfz5chcvjfm3r739yz9ic6i859ayd9v8i3y"))))
    (native-inputs
     (list pkg-config autoconf automake))
    (inputs
     (list fuse
	   libtorrent
	   libtorrent-rasterbar
	   openssl
	   boost
	   curl))
    (build-system gnu-build-system)
    (home-page "https://github.com/johang/btfs")
    (description
     "With BTFS, you can mount any .torrent file or magnet link and then use it as any read-only directory in your file tree. The contents of the files will be downloaded on-demand as they are read by applications. Tools like ls, cat and cp works as expected. Applications like vlc and mplayer can also work without changes.")
    (synopsis "A bittorrent filesystem based on FUSE. ")
    (license license:gpl3)))
