;;; GNU Guix --- Functional package management for GNU
;;;
;;; Copyright © 2021 konkrotte <konkrotte@posteo.net>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (kroko packages emacs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs))

(define-public emacs-ednc
  (let ((commit "b4f35672d4822be2985536c0d7531ca45d363039")
	(revision "0")
	(version "0.1"))
    (package
      (name "emacs-ednc")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
	       (url "https://github.com/sinic/ednc")
	       (commit commit)))
         (file-name (git-file-name name version))
         (sha256
	  (base32 "09s3id8gaxbp9bzq5n63n8nra1db45b8sa7rr7r50jgbsrrbb14z"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/sinic/ednc")
      (synopsis "The Emacs Desktop Notification Center ")
      (description "The Emacs Desktop Notification Center (EDNC) is an Emacs package written in pure Lisp that implements a Desktop Notifications service according to the freedesktop.org specification.")
      (license license:gpl3+))))

(define-public emacs-vlfi
  (let ((version "1.7.1"))
    (package
      (name "emacs-vlfi")
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
	       (url "https://github.com/m00natic/vlfi")
	       (commit version)))
         (file-name (git-file-name name version))
         (sha256
	  (base32 "0ziz08ylhkqwj2rp6h1z1yi309f6791b9r91nvr255l2331481pm"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/m00natic/vlfi")
      (synopsis "View Large Files in Emacs")
      (description "Emacs minor mode that allows viewing, editing, searching and comparing large files in batches, trading memory for processor time.")
      (license license:gpl2+))))

(define-public emacs-osm
  (let ((version "0.6"))
    (package
      (name "emacs-osm")
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
	       (url "https://github.com/minad/osm")
	       (commit version)))
         (file-name (git-file-name name version))
         (sha256
	  (base32 "0aiq2z9vv4jsl0s0x9vpjgp0mnn27wanhirzj3h80ivgiphzs7l5"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/minad/osm")
      (synopsis "OpenStreetMap viewer for Emacs")
      (description synopsis)
      (license license:gpl3+))))

(define-public emacs-logos
  (let ((version "0.3.1"))
    (package
      (name "emacs-logos")
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
	       (url "https://gitlab.com/protesilaos/logos")
	       (commit version)))
         (file-name (git-file-name name version))
         (sha256
	  (base32 "1xhnhaxmjqdv0bbh22gj9ak83hha8d59q64b6aa4rynrgcyajk45"))))
      (build-system emacs-build-system)
      (home-page "https://gitlab.com/protesilaos/logos")
      (synopsis "simple \"focus mode\"")
      (description
       "This package provides a simple “focus mode” which can be applied to
any buffer for reading, writing, or even doing a presentation. The
buffer can be divided in pages using the @code{page-delimiter},
outline structure, or any other pattern. Commands are provided to move
between those pages. These motions work even when narrowing is in
effect (and they preserve it). @code{logos.el} is designed to be
simple by default and easy to extend. This manual provides concrete
examples to that end.")
      (license license:gpl3+))))

(define-public emacs-lin
  (let ((version "0.3.0"))
    (package
      (name "emacs-lin")
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
	       (url "https://gitlab.com/protesilaos/lin")
	       (commit version)))
         (sha256
	  (base32 "1w1mli2wrxbnwagn3rx5ygslmzlri3drm74nqgwpl4pwh66xi98a"))))
      (build-system emacs-build-system)
      (home-page "https://gitlab.com/protesilaos/lin")
      (synopsis "Stylistic enhancement for Emacs’ built-in hl-line-mode")
      (description
       "Lin is a stylistic enhancement for Emacs’ built-in
@code{hl-line-mode}. It remaps the @code{hl-line} face (or equivalent)
buffer-locally to a style that is optimal for major modes where line
selection is the primary mode of interaction.")
      (license license:gpl3+))))
