(define-module (kroko packages audio)
  #:use-module ((nonguix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages wine)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk))

;; TODO: package yabridge
;; TODO: package bitwig
;; TODO: package pipe-jack

;; FIXME: reaper segfaults when loading lv2 instrument
(define-public reaper
  (package
    (name "reaper")
    (version "6.61")
    (source
     (origin
       (method url-fetch)
       (uri "https://www.reaper.fm/files/6.x/reaper661_linux_x86_64.tar.xz")
       (sha256
        (base32 "117lnkxqfxvkarj1ffvli6hsr234w5mk5pd2yffbpymm9ib8979f"))))
    (build-system copy-build-system)
    (inputs
     `(("libc" ,glibc)
       ("libx11" ,libx11)
       ("libxi" ,libxi)
       ("pcre" ,pcre)
       ("glib" ,glib)
       ("fontconfig" ,fontconfig)
       ("gcc:lib" ,gcc "lib")
       ("alsa-lib" ,alsa-lib)
       ("gtk+" ,gtk+)
       ;; ("lv2" ,lv2)
       ("pulseaudio" ,pulseaudio)
       ("libglvnd" ,libglvnd)))
    (native-inputs
     `(("patchelf" ,patchelf)
       ("which" ,which)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'build)
         (delete 'check)
         (delete 'strip)
         (replace 'install
           (lambda* (#:key inputs native-inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (libdir (string-append out "/lib")))
               (invoke "sh" "install-reaper.sh" "--install" libdir)
               (delete-file (string-append out "/lib/REAPER/uninstall-reaper.sh"))
               (mkdir (string-append out "/bin"))
               (symlink (string-append out "/lib/REAPER/reaper") (string-append out "/bin/reaper")))))
         (add-after 'install 'patchelf
           (lambda* (#:key inputs native-inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (libdir (string-append out "/lib")))
               (let* ((libc (assoc-ref inputs "libc"))
                      (ld.so (string-append libc ,(glibc-dynamic-linker)))
                      (out (assoc-ref outputs "out"))
                      (rpath (string-join
                              (list "$ORIGIN"
                                    (string-append out "/lib")
                                    (string-append libc "/lib")
                                    (string-append (assoc-ref inputs "gcc:lib") "/lib")
                                    (string-append (assoc-ref inputs "libglvnd") "/lib")
                                    (string-append (assoc-ref inputs "libxi") "/lib")
                                    (string-append (assoc-ref inputs "glib") "/lib")
                                    (string-append (assoc-ref inputs "gtk+") "/lib")
                                    (string-append (assoc-ref inputs "fontconfig") "/lib")
                                    ;; (string-append (assoc-ref inputs "lv2") "/lib")
                                    (string-append (assoc-ref inputs "alsa-lib") "/lib")
                                    (string-append (assoc-ref inputs "pulseaudio") "/lib")
                                    (string-append (assoc-ref inputs "libx11") "/lib"))
                              ":")))
                                        ; add needed libs
                 (invoke "patchelf" "--add-needed" "libgdk-3.so" (string-append out "/lib/REAPER/libSwell.so"))
                 (define (patch-elf file)
                   (format #t "Patching ~a ...~%" file)
                   (unless (string-contains file ".so")
                     (system* "patchelf" "--set-interpreter" ld.so file))
                   (invoke "patchelf" "--set-rpath" rpath file))
                 (for-each (lambda (file)
                             (when (elf-file? file)
                               (patch-elf file)))
                           (append (list (string-append out "/lib/REAPER/Plugins/reaper_host_x86_64")
                                         (string-append out "/lib/REAPER/reamote-server")
                                         (string-append out "/lib/REAPER/reaper"))
                                   (find-files out ".*\\.so"))))))))))
    (home-page "https://reaper.fm")
    (synopsis "A complete digital audio production application for computers")
    (description "REAPER is a complete digital audio production application for
                 computers, offering a full multitrack audio and MIDI recording,
                 editing, processing, mixing and mastering toolset.")
    (license (license:nonfree "https://reaper.fm/index.php"))))
